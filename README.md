Generate Command
================

Automatically generates Command/Handler and tests

# Instalation

Add the following code the repositories of your composer.json file

    {
        "type": "vcs",
        "url": "git@bitbucket.org:NaNtrack/generate-command"
    }

Run the following command in your terminal

    composer require --dev nantrack/generate-command

# Usage

    vendor/bin/generate-command
