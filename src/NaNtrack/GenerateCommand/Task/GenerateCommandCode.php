<?php

namespace NaNtrack\GenerateCommand\Task;

/**
 * Generate all the classes/files needed to run a Command and all the tests.
 */
class GenerateCommandCode extends AbstractTask
{
    protected $rootPath = null;
    protected $commandName = null;
    protected $directoryName = null;
    protected $fields = null;
    protected $namespace = null;

    public function run()
    {
        $this->appPath = dirname(dirname(dirname(dirname(dirname(dirname(dirname(__DIR__)))))));
        $this->rootPath = dirname(dirname(dirname(dirname(__DIR__))));
        $this->commandName = readline('Enter the Command Name: ');
        $this->directoryName = readline('Enter the Directory Name: ');
        $this->namespace = readline('Enter the NameSpace (some\nameSpace): ');
        $this->fields = readline('Enter the arguments in format(var1:type1[|null][:default],var2:type2...): ');

        $this->writeTestCommandClass();
        $this->writeTestHandlerClass();
        $this->writeCommandClass();
        $this->writeHandlerClass();
    }

    protected function writeCommandClass()
    {
        $builder = new \NaNtrack\GenerateCommand\Generator\Builder\Command();
        $builder->setOutputName($this->commandName.'Command.php');
        $builder->setVariable('className', $this->commandName.'Command');
        foreach (explode(',', $this->fields) as $f) {
            $fp = explode(':', $f);
            if (count($fp) > 1) {
                $name = trim($fp[0]);
                $type = str_replace(' ', '', trim($fp[1]));
                $default = isset($fp[2]) ? trim($fp[2]) : '_NULL_';
                $builder->addVar($name, $type, $default);
            }
        }

        $generator = new \TwigGenerator\Builder\Generator();
        $generator->setTemplateDirs(array(
            $this->rootPath.'/templates/generators',
        ));
        $generator->setVariables(array(
            'namespace' => "{$this->namespace}\Command\\{$this->directoryName}",
            'use' => $this->namespace,
            'implements' => 'CommandInterface',
            'directory' => $this->directoryName,
        ));
        $generator->addBuilder($builder);
        $generator->writeOnDisk($this->appPath.'/src/'.str_replace('\\','/', $this->namespace).'/Command/'.$this->directoryName);
    }

    protected function writeTestCommandClass()
    {
        $builder = new \NaNtrack\GenerateCommand\Generator\Builder\TestCommand();
        $builder->setOutputName($this->commandName.'CommandTest.php');
        $builder->setVariable('className', $this->commandName.'CommandTest');
        foreach (explode(',', $this->fields) as $f) {
            $fp = explode(':', $f);
            if (count($fp) > 1) {
                $builder->addVar(trim($fp[0]), trim($fp[1]));
            }
        }

        $generator = new \TwigGenerator\Builder\Generator();
        $generator->setTemplateDirs(array(
            $this->rootPath.'/templates/generators',
        ));
        $generator->setVariables(array(
            'namespace' => "{$this->namespace}\Test\Unit\Command\\{$this->directoryName}",
            'use' => $this->namespace,
            'extends' => 'AbstractUnitTest',
            'commandClass' => $this->commandName.'Command',
            'directory' => $this->directoryName,
        ));
        $generator->addBuilder($builder);
        $generator->writeOnDisk($this->appPath.'/tests/Unit/Command/'.$this->directoryName);
    }

    protected function writeHandlerClass()
    {
        $builder = new \NaNtrack\GenerateCommand\Generator\Builder\Handler();
        $builder->setOutputName($this->commandName.'Handler.php');
        $builder->setVariable('className', $this->commandName.'Handler');

        $generator = new \TwigGenerator\Builder\Generator();
        $generator->setTemplateDirs(array(
            $this->rootPath.'/templates/generators',
        ));
        $generator->setVariables(array(
            'namespace' => "{$this->namespace}\Handler\\{$this->directoryName}",
            'use' => $this->namespace,
            'extends' => 'AbstractHandler',
            'commandClass' => $this->commandName.'Command',
            'directory' => $this->directoryName,
        ));
        $generator->addBuilder($builder);
        $generator->writeOnDisk($this->appPath.'/src/'.str_replace('\\','/', $this->namespace).'/Handler/'.$this->directoryName);
    }

    protected function writeTestHandlerClass()
    {
        $builder = new \NaNtrack\GenerateCommand\Generator\Builder\TestHandler();
        $builder->setOutputName($this->commandName.'HandlerTest.php');
        $builder->setVariable('className', $this->commandName.'HandlerTest');
        foreach (explode(',', $this->fields) as $f) {
            $fp = explode(':', $f);
            if (count($fp) > 1) {
                $builder->addVar(trim($fp[0]), trim($fp[1]));
            }
        }

        $generator = new \TwigGenerator\Builder\Generator();
        $generator->setTemplateDirs(array(
            $this->rootPath.'/templates/generators',
        ));
        $generator->setVariables(array(
            'namespace' => "{$this->namespace}\Test\Unit\Handler\\{$this->directoryName}",
            'use' => $this->namespace,
            'extends' => 'AbstractUnitTest',
            'handlerClass' => $this->commandName.'Handler',
            'commandClass' => $this->commandName.'Command',
            'directory' => $this->directoryName,
        ));
        $generator->addBuilder($builder);
        $generator->writeOnDisk($this->appPath.'/tests/Unit/Handler/'.$this->directoryName);
    }
}
