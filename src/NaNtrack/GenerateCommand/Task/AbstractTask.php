<?php

namespace NaNtrack\GenerateCommand\Task;

abstract class AbstractTask
{
  abstract public function run();
}
