<?php

namespace NaNtrack\GenerateCommand\Generator\Builder;

use TwigGenerator\Builder\BaseBuilder;

class Command extends BaseBuilder
{
    protected $mustOverwriteIfExists = false;
    protected $vars = [];

    public function addVar($name, $type, $default)
    {
        $this->vars[] = [
            'name' => $name,
            'type' => $type,
            'default' => $default
            ];
    }

    public function getArguments()
    {
        return array_map(function ($v) {
            $default = $v['default'] !== '_NULL_' ? ' = ' . $v['default'] : '';
            if (strpos($v['type'], '|null')) {
                return '$'.$v['name'].$default;
            }
            return $v['type'].' $'.$v['name'].$default;
        }, $this->vars);
    }

    public function getVariables()
    {
        return array_merge(parent::getVariables(), [
            'vars' => $this->vars,
            'arguments' => $this->getArguments(),
        ]);
    }
}
