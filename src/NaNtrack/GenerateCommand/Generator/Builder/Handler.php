<?php

namespace NaNtrack\GenerateCommand\Generator\Builder;

use TwigGenerator\Builder\BaseBuilder;

class Handler extends BaseBuilder
{
    protected $mustOverwriteIfExists = false;
}
