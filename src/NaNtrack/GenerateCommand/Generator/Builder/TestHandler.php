<?php

namespace NaNtrack\GenerateCommand\Generator\Builder;

use TwigGenerator\Builder\BaseBuilder;

class TestHandler extends BaseBuilder
{
    protected $mustOverwriteIfExists = false;

    protected $vars = [];

    public function addVar($name, $type)
    {
        $this->vars[] = [
            'name' => $name,
            'type' => $type,
            ];
    }

    public function getParameters()
    {
        return array_map(function ($v) {
            return "'".$v['name']."'";
        }, $this->vars);
    }

    public function getVariables()
    {
        return array_merge(parent::getVariables(), [
            'vars' => $this->vars,
            'parameters' => $this->getParameters(),
        ]);
    }
}
