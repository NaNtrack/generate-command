<?php

require 'vendor/autoload.php';

$options = getopt('', ['task:']);

if (!isset($options['task'])) {
    die("Must specify a --task parameter\n");
}

$reflector = new ReflectionClass('\\NaNtrack\\GenerateCommand\\Task\\'.$options['task']);
$task = $reflector->newInstance();
$task->run();
